# Terraform

This repository creates an Ubuntu machine on AWS.

## Overview

```text
+--- Cloudflare record -----------+
| {{ deployment_name }}.meinit.nl |
+---------------------------------+
       |
       V
+--- AWS lb ---+
|              |
+--------------+
       |
       V
+--- AWS instance ----------------------------+
| {{ deployment_name }}-{{ count }}.meinit.nl |+
+---------------------------------------------+|
 +---------------------------------------------+
```

## Preparation

Please save these variable to allow Terraform to use AWS:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`

To use Cloudflare, set these variables:

- `TF_VAR_cloudflare_api_token`

That token should have at least these permissions:

- Zone:zone:read
- Zone:dns:edit

## Running

```shell
terraform init
terraform apply
```

You (or Ansible) should now be able to login:

```shell
ssh -i ssh_keys/id_rsa ubuntu@demo.meinit.nl
```
