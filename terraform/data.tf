data "aws_ami" "default" {
  most_recent = true
  filter {
    name   = "name"
    values = ["Fedora-Cloud-Base-34-1.2.x86_64-hvm-eu-west-1-gp2-0"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["125523088429"]
}
