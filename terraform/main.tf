resource "tls_private_key" "default" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "default" {
  key_name   = var.deployment_name
  public_key = tls_private_key.default.public_key_openssh
  tags       = {}
}

resource "local_file" "id_rsa" {
  content              = templatefile("templates/id_rsa.tpl", { id_rsa_key = tls_private_key.default.private_key_pem })
  filename             = "${path.module}/../ssh_keys/id_rsa"
  directory_permission = "0750"
  file_permission      = "0400"
}

resource "aws_vpc" "default" {
  cidr_block           = "172.16.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags                 = {}
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id
}

resource "aws_subnet" "default" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = "172.16.10.0/24"
  availability_zone = "eu-west-1a"
  tags              = {}
}

resource "aws_security_group" "internet" {
  name        = "internet"
  description = "Allow outbound traffic"
  vpc_id      = aws_vpc.default.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {}
}

resource "aws_security_group" "ssh" {
  # TODO: for_echt ssh http https
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.default.id

  ingress {
    description      = "SSH from anywhere"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {}
}

resource "aws_security_group" "http" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_vpc.default.id

  ingress {
    description      = "HTTP from anywhere"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {}
}

resource "aws_security_group" "https" {
  name        = "allow_https"
  description = "Allow HTTPS inbound traffic"
  vpc_id      = aws_vpc.default.id

  ingress {
    description      = "HTTPS from anywhere"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {}
}

resource "aws_instance" "default" {
  count                  = var.instances
  ami                    = data.aws_ami.default.id
  # instance_type          = "t3.small"
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.internet.id, aws_security_group.ssh.id, aws_security_group.http.id, aws_security_group.https.id]
  subnet_id              = aws_subnet.default.id
  key_name               = aws_key_pair.default.key_name
  tags                   = {}
}

resource "aws_lb" "default" {
  name                             = var.deployment_name
  load_balancer_type               = "network"
  subnets                          = [aws_subnet.default.id]
  enable_cross_zone_load_balancing = true
}

resource "aws_lb_target_group" "default" {
  port     = 443
  protocol = "TCP"
  vpc_id   = aws_vpc.default.id
  depends_on = [
    aws_lb.default
  ]
}

resource "aws_lb_listener" "default" {
  for_each          = toset(["80", "443"])
  load_balancer_arn = aws_lb.default.arn
  protocol          = "TCP"
  port              = each.value
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.default.arn
  }
}

resource "aws_lb_target_group_attachment" "http" {
  count            = var.instances
  target_group_arn = aws_lb_target_group.default.arn
  target_id        = aws_instance.default[count.index].id
  port             = 80
}

resource "aws_lb_target_group_attachment" "https" {
  count            = var.instances
  target_group_arn = aws_lb_target_group.default.arn
  target_id        = aws_instance.default[count.index].id
  port             = 443
}

resource "aws_eip" "default" {
  count    = var.instances
  instance = aws_instance.default.*.id[count.index]
  vpc      = true
  tags     = {}
}

resource "aws_route_table" "default" {
  vpc_id = aws_vpc.default.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
  }
  tags = {}
}

resource "aws_route_table_association" "default" {
  subnet_id      = aws_subnet.default.id
  route_table_id = aws_route_table.default.id
}

data "cloudflare_zones" "default" {
  filter {
    name = "meinit.nl"
  }
}

resource "cloudflare_record" "default" {
  count   = var.instances
  zone_id = data.cloudflare_zones.default.zones[0].id
  name    = "${var.deployment_name}-${count.index}"
  value   = aws_eip.default.*.public_ip[count.index]
  type    = "A"
  ttl     = 300
}

resource "cloudflare_record" "loadbalanced" {
  zone_id = data.cloudflare_zones.default.zones[0].id
  name    = var.deployment_name
  value   = aws_lb.default.dns_name
  type    = "CNAME"
  proxied = true
  ttl     = 1
}

resource "local_file" "hosts" {
  content              = templatefile("templates/hosts.tpl", { hosts = cloudflare_record.default.*.hostname })
  filename             = "${path.module}/../ansible/hosts"
  directory_permission = "0755"
  file_permission      = "0644"
}
