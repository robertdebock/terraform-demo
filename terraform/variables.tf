variable "deployment_name" {
  type    = string
  default = "demo"
  validation {
    condition     = length(var.deployment_name) >= 1 && length(var.deployment_name) <= 16
    error_message = "The deployment_name value must 1 to 16 characters."
  }
}

variable "instances" {
  type    = number
  default = 1
  validation {
    condition     = var.instances >= 1
    error_message = "Please use a (positive) number of instances."
  }
}
