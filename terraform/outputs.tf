output "loadbalancer_address" {
  value = aws_lb.default.dns_name
}
