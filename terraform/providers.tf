provider "aws" {
  region = "eu-west-1"
}

variable "cloudflare_api_token" {
  type        = string
  description = "The CloudFlare API Token to manage resources."
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}
