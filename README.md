# Terraform demo



![Morpheus](https://timberry.dev/images/1_p4Yqglt2-fmLnGanM-vs2g.jpeg "Take a pill")


A combination of:

- GitLab (CI/CD)
- Terraform
- Ansible

This demo uses GitLab (CI/pipelines) to:

1. Create the infrastructure on AWS using Terraform.
2. Configure the infrastructure using Ansible.

```text
                             \O/
                              | "Okay!"
                             / \
 O      +--- GitLab ------+   |
/|\---> | - Code          |   V  +--- AWS ----+
/ \     | - gitlab-ci.yml | ---> | - resouces |
DEV     | - Variables     |      +------------+
        +-----------------+
```

## Overview

This repository creates a [couple of](https://gitlab.com/robertdebock/terraform-demo/-/blob/master/terraform/terraform.tfvars) instances. The generated URL in [configurable](https://gitlab.com/robertdebock/terraform-demo/-/blob/master/terraform/terraform.tfvars).

```text
   +--- aws_lb ---+
   |              |
   +--------------+
          |
          V
+--- aws_instance ---+
|                    |+
+--------------------+|
 +--------------------+
 ```
