# Ansible

Configure machines to become webservers.

## Prepare

```shell
ansible-galaxy install -r roles/requirements.yml
ansible-galaxy collection install -r roles/requirements.yml
```

## Running

```shell
./playbook.yml
```
